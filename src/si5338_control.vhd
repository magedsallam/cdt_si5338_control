--------------------------------------------------------------------------------
--  ESS Detector Readout Project
--  Nauman Iqbal     RAL - 2019 
--
--
--
--  Description : Initialise Si570 to requested frequency
--
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;


library UNISIM;
use UNISIM.VComponents.all;

entity si5338_ctrl is
generic (
  I2C_INPUT_CLK_G : INTEGER := 100_000_000; -- Input clock [Hz]
  I2C_BUS_CLK_G   : INTEGER := 100_000;     -- I2C clock [Hz]
  I2C_THROTTLE_G  : INTEGER := 200_000;     -- Sysclk's between I2C pkts, 120_000
  ILA_DEBUG_ON_G  : string  := "false"
); 
port (
    -- system clock    
    CLK50MHZ_P               : in    std_logic;
    CLK50MHZ_N               : in    std_logic;
--    MGT_CLK2_P               : in    std_logic;
--    MGT_CLK2_N               : in    std_logic;
--    RESET_I                 : in    std_logic;
    -- I2C Interface
    sda                     : inout std_logic;
    scl                     : inout std_logic;
    MGT_CLK_P         : IN    std_logic;
    MGT_CLK_N         : IN    std_logic;
    CLK10MHz           :in    std_logic;
    LED_O                   : out   std_logic
--    I2C_START_i             : IN    std_logic;
--    I2C_DONE_O              : out   std_logic

);
end si5338_ctrl;

architecture rtl of si5338_ctrl is

type i2c_fsm_t is (INIT,disable_outputs,SYNC0,LOL,SYNC1,READ_INIT,SYNC2,READ,MASKING,SYNC3,WRITE,index_comp,wait_100ms,SYNC4,initiate_locking,wait_100ms2,SYNC5,restart_lol,wait_100ms3,read218_init,SYNC_read218,read218,SYNC6,use_FCAL,SYNC7,enable_outputs,FINISHED);
type i2c_mem_t is array (natural range <>) of std_logic_vector(15 downto 0);
type i2c_data_t is array (natural range <>) of std_logic_vector(23 downto 0);
type i2c_data_t2 is array (natural range <>) of std_logic_vector(7 downto 0);

--------------------------------------------------------------------
-- I2C Read/Write operations are played from memory,
-- Memory Content stores address and value in 16-bit format.
-- bits[15:8] => Register address
-- bits[ 7:0] => Register value to be written
--------------------------------------------------------------------
-- Play Memory depth
constant CONF_SIZE          : integer := 6; -- number of registers in main config
constant FMETER_CHANNELS    : integer := 5;
constant FMETER_NUM_BITS    : integer := 32;

signal i2c_mem            : i2c_mem_t(0 to CONF_SIZE-1);
signal si5338_regmap        : i2c_data_t(0 to 111);
signal index              : integer:=0; 
signal Masking_reg        : std_logic_vector (7 downto 0);
signal vio_start          : std_logic;

signal CLK_I            : std_logic;
signal locked           : std_logic;
signal i2c_done         : std_logic;
signal bufgcounter      : integer:= 0;
signal CLK_158_bufg2    : std_logic;
signal CLK_IN_158MHz2   : std_logic;
signal masked_old_value : std_logic_vector(7 downto 0);
signal masked_new_value : std_logic_vector(7 downto 0);

signal clk_250MHz       : std_logic;
--signal CLK_158_bufggt   : std_logic;

signal i2c_fsm          : i2c_fsm_t;
signal next_state       : i2c_fsm_t;
signal busy_prev        : std_logic;
signal i2c_ena          : std_logic;
signal i2c_addr         : std_logic_vector(6 downto 0);
signal i2c_rw           : std_logic;
signal i2c_data_wr      : std_logic_vector(7 downto 0);
signal i2c_busy         : std_logic;
signal i2c_data_rd      : std_logic_vector(7 downto 0);
signal i2c_ack_error    : std_logic;
signal i2c_rise         : std_logic;
signal i2c_data         : i2c_data_t(7 downto 0);
signal i2c_start        : std_logic;
signal sda_din          : std_logic;
signal scl_din          : std_logic;
signal sda_t            : std_logic;
signal scl_t            : std_logic;
signal RESET_I          : std_logic:= '0';
signal CLK_IN_158MHz    : std_logic;
signal CLK_158_bufg     : std_logic;
signal CLK50MHZ         : std_logic;
signal maskreg_test     : std_logic_vector (7 downto 0);
signal waitcounter      : integer :=0; 


signal i2c_play         : std_logic;
signal windex           : integer range 0 to CONF_SIZE-1;


signal fin              : std_logic_vector(FMETER_CHANNELS-1 downto 0);
signal fout0            : std_logic_vector(FMETER_NUM_BITS-1 downto 0);
signal fout1            : std_logic_vector(FMETER_NUM_BITS-1 downto 0);
signal fout2            : std_logic_vector(FMETER_NUM_BITS-1 downto 0);
signal fout3            : std_logic_vector (FMETER_NUM_BITS-1 downto 0);
signal fout4            : std_logic_vector (FMETER_NUM_BITS-1 downto 0);
signal update           : std_logic_vector(0 downto 0);


attribute mark_debug     : string;
constant  dbg_state      : string := "true";
attribute mark_debug of sda_din : signal is dbg_state;
attribute mark_debug of scl_din : signal is dbg_state;
attribute mark_debug of sda_t   : signal is dbg_state;
attribute mark_debug of scl_t   : signal is dbg_state;
attribute mark_debug of windex  : signal is dbg_state;
attribute mark_debug of i2c_fsm : signal is dbg_state;
attribute mark_debug of i2c_data_rd    : signal is dbg_state;
attribute mark_debug of i2c_ack_error  : signal is dbg_state;
attribute mark_debug of index : signal is dbg_state;



component clk_wiz_0
port
 (-- Clock in ports
  -- Clock out ports
  clk_out1          : out    std_logic;


  -- Status and control signals
  reset             : in     std_logic;
  locked            : out    std_logic;
  clk_in1          : in     std_logic


 );
end component;

COMPONENT vio_freq_meter
  PORT (
    clk : IN STD_LOGIC;
    probe_in0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_in1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_in2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_in3 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_in4 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_in5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
  );
END COMPONENT;

COMPONENT vio_0
  PORT (
    clk : IN STD_LOGIC;
    probe_out0 : out STD_LOGIC
  );
END COMPONENT;



begin
-- Register map for setting si5338 output 2 to 158.4945MHz  (output @ 50MHz) 
si5338_regmap<=
(
x"34107F",
x"1F001C",
x"20001C",
x"21001C",
x"22001C",
x"4000FF",
x"4117FF",
x"4200FF",
x"4300FF",
x"4400FF",
x"4500FF",
x"4601FF",
x"4700FF",
x"4800FF",
x"49003F",
x"4A107F",
x"4BE2FF",
x"4C05FF",
x"4D98FF",
x"4E4CFF",
x"4F13FF",
x"5000FF",
x"513DFF",
x"52D6FF",
x"5304FF",
x"54003F",
x"5600FF",
x"5717FF",
x"5800FF",
x"5900FF",
x"5A00FF",
x"5B00FF",
x"5C01FF",
x"5D00FF",
x"5E00FF",
x"5F003F",
x"6100FF",
x"6230FF",
x"6300FF",
x"6400FF",
x"6500FF",
x"6600FF",
x"6701FF",
x"6800FF",
x"6900FF",
x"6A80BF",
x"9800FF",
x"9900FF",
x"9A00FF",
x"9B00FF",
x"9C00FF",
x"9D00FF",
x"9E000F",
x"9F000F",
x"A000FF",
x"A100FF",
x"A200FF",
x"A300FF",
x"A400FF",
x"A500FF",
x"A600FF",
x"A700FF",
x"A800FF",
x"A900FF",
x"AA00FF",
x"AB00FF",
x"AC00FF",
x"AD00FF",
x"AE00FF",
x"AF00FF",
x"B000FF",
x"B100FF",
x"B200FF",
x"B300FF",
x"B400FF",
x"B5000F",
x"B600FF",
x"B700FF",
x"B800FF",
x"B900FF",
x"BA00FF",
x"BB00FF",
x"BC00FF",
x"BD00FF",
x"BE00FF",
x"BF00FF",
x"C000FF",
x"C100FF",
x"C200FF",
x"C300FF",
x"C400FF",
x"C500FF",
x"C600FF",
x"C700FF",
x"C800FF",
x"C900FF",
x"CA00FF",
x"CB000F",
x"CC00FF",
x"CD00FF",
x"CE00FF",
x"CF00FF",
x"D000FF",
x"D100FF",
x"D200FF",
x"D300FF",
x"D400FF",
x"D500FF",
x"D600FF",
x"D700FF",
x"D800FF",
x"D900FF"
);

 

------------------------------------------------------------------------------
clk_gen : clk_wiz_0
   port map ( 
        -- Clock out ports  
        clk_out1 => CLK_I,
        -- Status and control signals                
        reset => RESET_I,
        locked => locked,
        -- Clock in ports
        clk_in1 => CLK10MHz
  

 );


    IBUFDS_GTE2_inst_si5338: IBUFDS_GTE2
generic map(
CLKRCV_TRST =>TRUE,
CLKCM_CFG => TRUE,
CLKSWING_CFG =>"11"
)
port map (
    O               => CLK_IN_158MHz,
    I               => MGT_CLK_P,
    IB              => MGT_CLK_N,
    CEB             => '0',
    ODIV2           => open
);

    
blinky_1 : entity work.blinky
    Generic map (
        COUNTVAL => 25 ) 
    Port map (
        CLK_I   => CLK_IN_158MHz,
        LED_O   => LED_O 
    );
    
 IBUFDS_inst:IBUFDS
 generic map(
 DIFF_TERM=>FALSE,--DifferentialTermination
 IBUF_LOW_PWR=>TRUE,--Lowpower(TRUE)vs.performance(FALSE)settingforreferencedI/Ostandards
 IOSTANDARD=>"DEFAULT"
 )
 port map
 (O => CLK50MHZ,--Bufferoutput
  I => CLK50MHZ_P,--Diff_pbufferinput(connectdirectlytotop-levelport)
  IB=> CLK50MHZ_N--Diff_nbufferinput(connectdirectlytotop-levelport)
  );
    
-----------------------------------------------------------------
-- Frequency meter
-----------------------------------------------------------------
fin(0) <= CLK_I;
fin(1) <= CLK_IN_158MHz;
fin(2) <= CLK10MHz;
fin(3) <= CLK50MHZ;
--fin(4) <= CLK_158_bufg2;

fmeter : entity work.labtools_fmeter
    generic map (
        C_REFCLK_HZ => 100000000,
        C_CHANNELS  => FMETER_CHANNELS,
        C_MODE      => 0,
        C_NUM_BITS  => FMETER_NUM_BITS
    )  
    port map ( 
        refclk => CLK_I,
        fin    => fin,
        
        F0    => fout0(FMETER_NUM_BITS -1 downto 0),
        F1    => fout1(FMETER_NUM_BITS -1 downto 0),
        F2    => fout2(FMETER_NUM_BITS -1 downto 0),
        F3    => fout3(FMETER_NUM_BITS -1 downto 0),
        F4    => open,
        F5    => open,
        F6    => open,
        F7    => open,            
        F8    => open,
        F9    => open,
        F10   => open,
        F11   => open,
        F12   => open,
        F13   => open,
        F14   => open,
        F15   => open,            
        F16   => open,
        F17   => open,
        F18   => open,
        F19   => open,            
        F20   => open,
        F21   => open,
        F22   => open,
        F23   => open,            
        F24   => open,
        F25   => open,
        F26   => open,
        F27   => open,
        F28   => open,
        F29   => open,
        F30   => open,
        F31   => open,            
        update =>  update(0)
    );
    
   vi0_start: vio_0
  port map (
  clk=> CLK_I,
  probe_out0=> vio_start
   );

vio_ip : vio_freq_meter
    PORT MAP (
        clk       => CLK_I,
        probe_in0 => fout0,
        probe_in1 => fout1,
        probe_in2 => fout2,
        probe_in3 => fout3,
        probe_in4 => fout4,
        probe_in5 => update(0 downto 0)
    );

--------------------------------------------------------------------
-- 3-state I2C I/O Buffers
--------------------------------------------------------------------
iobuf_scl : iobuf
port map (
    I  => '0',
    O  => scl_din,
    IO => scl,
    T  => scl_t
);

iobuf_sda : iobuf
port map (
    I  => '0',
    O  => sda_din,
    IO => sda,
    T  => sda_t
);

--------------------------------------------------------------------
-- I2C Master Device 
--------------------------------------------------------------------
i2c_master_inst : entity work.i2c_master
generic map (
    input_clk   => I2C_INPUT_CLK_G,
    bus_clk     => I2C_BUS_CLK_G
)
port map (
    clk         => CLK_I,
    reset       => RESET_I,
    ena         => i2c_ena,
    addr        => i2c_addr,
    rw          => i2c_rw,
    data_wr     => i2c_data_wr,
    busy        => i2c_busy,
    data_rd     => i2c_data_rd,
    ack_error   => i2c_ack_error,
    sda         => sda_din,
    scl         => scl_din,
    sda_t       => sda_t,
    scl_t       => scl_t
);

-- Throttle down I2C packets just to be on the safe side
start_presc : entity work.prescaler
generic map (
    I2C_THROTTLE_G  => I2C_THROTTLE_G     -- Sysclk's between I2C pkts, 120_000;     -- Sysclk's between I2C pkts, 120_000
    )
port map (
    clk_i       => CLK_I,
    reset_i     => RESET_I,
    pulse_o     => i2c_start
);

--------------------------------------------------------------------
-- Main read state machine loops through all SLAVES
--------------------------------------------------------------------
i2c_rise <= i2c_busy and not busy_prev;


process(clk_i)
    variable busy_cnt         : natural range 0 to 15;
    variable count_delay      : natural range 0 to 30000000;
begin
    if rising_edge(clk_i) then
        if (reset_i = '1') then
            i2c_fsm     <= INIT;
            windex      <= 0;
            busy_cnt    := 0;
            busy_prev   <= '1';
            i2c_rw      <= '1';
            i2c_ena     <= '0';
            i2c_addr    <= (others => '0');
            i2c_data_wr <= X"00";
--            I2C_DONE_O  <= '0';
        else
            busy_prev <= i2c_busy;

            case (i2c_fsm) is
                -- Wait I2C master to start
                when INIT =>
                    if (i2c_busy = '0' and i2c_start = '1' and vio_start = '1') then
                        i2c_fsm        <= disable_outputs;
       
                    end if;
                    
                    
                  when disable_outputs =>
                      if (i2c_rise = '1') then
                        busy_cnt := busy_cnt + 1;
                      end if;

                    case busy_cnt is
                        -- Latch slave address and register address
                        when 0 =>
                            i2c_ena     <= '1';
                            i2c_addr    <= "1110000";  -- I2C Addr of si5338 = 0x70
                            i2c_rw      <= '0';          -- write
                            i2c_data_wr <= "11100110" ;  -- register address

                        -- Latch register data    
                        when 1 =>
                            i2c_data_wr <= "00010000";  -- register value

                        -- Continue until all values are written
                        when 2 =>
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                i2c_fsm<= SYNC0;
                                
                            end if;

                         when others => NULL;
                        end case;
                        
                        when SYNC0 =>
                          if (i2c_busy = '0' and i2c_start = '1') then
                               i2c_fsm <= LOL;
                           end if;
                           
                   when LOL =>
                    if (i2c_rise = '1') then
                        busy_cnt := busy_cnt + 1;
                    end if;

                    case busy_cnt is
                        -- Latch slave address and register address
                        when 0 =>
                            i2c_ena     <= '1';
                            i2c_addr    <= "1110000";  -- I2C Addr 
                            i2c_rw      <= '0';          -- write
                            i2c_data_wr <= "11110001" ;  -- reg addr

                        -- Latch register data    
                        when 1 =>
                            i2c_data_wr <= "11100101"; -- reg new val

                        -- Continue until all values are written
                        when 2 =>
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                i2c_fsm<=SYNC1;
                                
                            end if;

                         when others => NULL;
                        end case;
                        
                        when SYNC1 =>
                          if (i2c_busy = '0' and i2c_start = '1') then
                               i2c_fsm <= read_init;
                               
                           end if;
                           
                when Read_init =>  -- Read-modify- write process of Register map  using given register masks
                     if (i2c_rise = '1') then
                        busy_cnt := busy_cnt + 1;
                    end if;
            
                    case busy_cnt is
                        when 0 =>
                            i2c_ena <= '1';
                            i2c_addr <= "1110000";  --  Address of S5338
                            i2c_rw <= '0';          -- Write 
                            i2c_data_wr <= si5338_regmap(index)(23 downto 16);   --address of vreg to be updated
                        when 1 =>            
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                i2c_fsm <= SYNC2;
                            end if;
                        when others => NULL;
                      end case;
                            
                      when SYNC2 =>
                          if (i2c_busy = '0' and i2c_start = '1') then
                               i2c_fsm <= read;
                           end if;
                           
                   when Read=>
                     if (i2c_rise = '1') then
                        busy_cnt := busy_cnt + 1;
                    end if;
            
                    case busy_cnt is
                        when 0 =>
                            i2c_ena <= '1';
                            i2c_addr <= "1110000";  --  Address of S5338
                            i2c_rw <= '1';          -- read

                        when 1 =>            
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                i2c_fsm <= Masking;
                              
                           
                            end if;
                        when others => NULL;
                      end case;
      
                     When MASKING=>
                     
                         masked_old_value <= i2c_data_rd and not si5338_regmap(index)(7 downto 0);
                         masked_new_value <=  si5338_regmap(index)(15 downto 8) and si5338_regmap(index)(7 downto 0) ;                       
                         i2c_fsm <= SYNC3;

                        
                     when SYNC3 =>
                          if (i2c_busy = '0' and i2c_start = '1') then
                               i2c_fsm <= Write;
                           end if;
                           
                    when Write =>
                    if (i2c_rise = '1') then
                        busy_cnt := busy_cnt + 1;
                    end if;

                    case busy_cnt is
                        -- Latch slave address and register address
                        when 0 =>
                            i2c_ena     <= '1';
                            i2c_addr    <= "1110000";  -- I2C Addr of si570 = 0x55
                            i2c_rw      <= '0';          -- write
                            i2c_data_wr <= si5338_regmap(index)(23 downto 16) ;  -- writing registers

                        -- Latch register data    
                        when 1 =>
                            i2c_data_wr <=  masked_old_value or  masked_new_value;

                        -- Continue until all values are written
                        when 2 =>
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                i2c_fsm<=index_comp;
                                
                            end if;

                         when others => NULL;
                        end case;
                        
                      When index_comp =>
                       if index < 111 then   -- when regmap is done go next step of programming the chip
                        index <= index + 1 ;
                        i2c_fsm<= SYNC1;
                        else
                        index <= 0;
                        i2c_fsm<= wait_100ms;
                       end if;
                      
                      when wait_100ms =>
                      
                      if waitcounter = 1000000 then
                        i2c_fsm <= SYNC4;
                        waitcounter<=0;
                      else
                        waitcounter <= waitcounter +1; 
                      end if;
                      
                      when SYNC4 =>
                        if (i2c_busy = '0' and i2c_start = '1') then
                               i2c_fsm <= initiate_locking;
                           end if;
                           

                      when initiate_locking =>
                                      
                           if (i2c_rise = '1') then
                             busy_cnt := busy_cnt + 1;
                            end if;

                       case busy_cnt is
                        -- Latch slave address and register address
                        when 0 =>
                            i2c_ena     <= '1';
                            i2c_addr    <= "1110000";  -- I2C Addr 
                            i2c_rw      <= '0';          -- write
                            i2c_data_wr <= x"F6" ;  -- writing registers

                        -- Latch register data    
                        when 1 =>
                            i2c_data_wr <= x"02";

                      
                        when 2 =>
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                i2c_fsm<=wait_100ms2;
                            end if;

                            when others => NULL;
                         end case;

                     when wait_100ms2 =>
                      
                      if waitcounter = 1000000 then
                        i2c_fsm <= SYNC5;
                        waitcounter<=0;
                      else
                        waitcounter <= waitcounter +1; 
                      end if;
                      
                      
                       when SYNC5 =>
                        if (i2c_busy = '0' and i2c_start = '1') then
                               i2c_fsm <= restart_lol;
                           end if;
                           
                           
                        when restart_lol =>
                                      
                           if (i2c_rise = '1') then
                             busy_cnt := busy_cnt + 1;
                            end if;

                       case busy_cnt is
                        -- Latch slave address and register address
                        when 0 =>
                            i2c_ena     <= '1';
                            i2c_addr    <= "1110000";  -- I2C Addr of si570 = 0x55
                            i2c_rw      <= '0';          -- write
                            i2c_data_wr <= x"F1" ;  -- writing registers

                        -- Latch register data    
                        when 1 =>
                            i2c_data_wr <= x"65";

                        -- Continue 
                        when 2 =>
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                i2c_fsm<=wait_100ms3;
                            end if;

                            when others => NULL;
                         end case;
                         
                         
                    when wait_100ms3 =>
                      
                      if waitcounter = 1000000 then
                        i2c_fsm <= SYNC6;
                        waitcounter<=0;
                      else
                        waitcounter <= waitcounter +1; 
                      end if;
                      
                        when SYNC6 =>
                        if (i2c_busy = '0' and i2c_start = '1') then
                               i2c_fsm <= read218_init;
                           end if;
                           
                 when read218_init =>  -- this is used for  status monitoring ( if return value is not 0) something is wrong
                      
                                 
                     if (i2c_rise = '1') then
                        busy_cnt := busy_cnt + 1;
                    end if;
            
                    case busy_cnt is
                        when 0 =>
                            i2c_ena <= '1';
                            i2c_addr <= "1110000";  --  Address of S5338
                            i2c_rw <= '0';          -- Write cmd
                            i2c_data_wr <= x"EB";   -- value of register to be read
                        when 1 =>            
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                i2c_fsm <= SYNC_read218;
                            end if;
                        when others => NULL;
                      end case;
                            
                      when SYNC_read218 =>
                          if (i2c_busy = '0' and i2c_start = '1') then
                               i2c_fsm <= read218;
                           end if;
                           
                       when read218 =>      -- this is used for chipscope  status monitoring ( if return value is not 0) something is wrong
                             if (i2c_rise = '1') then
                        busy_cnt := busy_cnt + 1;
                    end if;
            
                    case busy_cnt is
                        when 0 =>
                            i2c_ena <= '1';
                            i2c_addr <= "1110000";     --  Address of S5338
                            i2c_rw <= '1';          -- read

                        when 1 =>            
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                i2c_fsm <= use_FCAL;
                           
                            end if;
                        when others => NULL;
                      end case;
                           
                           
                        when use_FCAL=>   
                               
                           if (i2c_rise = '1') then
                             busy_cnt := busy_cnt + 1;
                            end if;

                       case busy_cnt is
                        -- Latch slave address and register address
                        when 0 =>
                            i2c_ena     <= '1';
                            i2c_addr    <= "1110000";  -- I2C Addr of si5338
                            i2c_rw      <= '0';          -- write
                            i2c_data_wr <= x"31" ;  -- writing registers

                        -- Latch register data    
                        when 1 =>
                            i2c_data_wr <= x"80";

             
                        when 2 =>
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                i2c_fsm<=SYNC7;
                            end if;

                            when others => NULL;
                         end case;
                         
                           when SYNC7 =>
                        if (i2c_busy = '0' and i2c_start = '1') then
                               i2c_fsm <= enable_outputs;
                           end if;

                           
                        when enable_outputs=>
                        
                                   
                           if (i2c_rise = '1') then
                             busy_cnt := busy_cnt + 1;
                            end if;

                       case busy_cnt is
                        -- Latch slave address and register address
                        when 0 =>
                            i2c_ena     <= '1';
                            i2c_addr    <= "1110000";  -- I2C Addr of si570 = 0x55
                            i2c_rw      <= '0';          -- write
                            i2c_data_wr <= x"E6" ;  -- writing registers

                        -- Latch register data    
                        when 1 =>
                            i2c_data_wr <= x"00";

                    
                        when 2 =>
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                i2c_fsm<=FINISHED;
                            end if;

                            when others => NULL;
                         end case;
                            
                       when FINISHED=>
                        i2c_done <='1';
                        
                     when others => NULL;
                 end case;
            
        end if;
end if;
end process;

RESET_I<='0';

end rtl;
