----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.05.2019 16:37:10
-- Design Name: 
-- Module Name: blinky - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

 

entity blinky is
Generic (
  COUNTVAL : integer := 26 ); --approx. 1 second for 100MHz clk
Port (
  CLK_I : in STD_LOGIC;
  LED_O : out STD_LOGIC );
end blinky;

 

architecture rtl of Blinky is

  signal count : unsigned(COUNTVAL downto 0) := (others => '0');
  signal LED_i : std_logic;
 
 attribute mark_debug     : string;
 constant dbg_state       : string := "false";
 attribute mark_debug of count : signal is dbg_state;

begin

--    LED_O <= LED_i;
    count_proc : process(CLK_I) is
    begin
        if rising_edge(CLK_I) then
            count <= count + 1;  
        end if;
    end process count_proc;

 

  LED_proc : process(CLK_I) is
  begin
    if rising_edge(CLK_I) then
      if count(count'left) = '1' then
        LED_O <= '1';
      else
        LED_O <= '0';
      end if;
    end if;
  end process LED_proc;

--process (CLK_I) is
--  begin
--    if rising_edge(CLK_I) then
--      if count = X"02540BE3" then  -- -1, since counter starts at 0
--        LED_i   <= not LED_i;
--        count <= X"00000000";
--      else
--        LED_i <= LED_i;
--        count <= count + 1;
--      end if;
--    end if;
--  end process;
  
 end;
 
 
 